@extends('layouts.base')

@section('block-header')
    <h2>
        Médicos
        <div class="float-right" style="margin-top: -6px">
            <a role="button" class="btn btn-raised btn-primary" href="{{ route('doctors.create') }}">
                Adicionar Médico
            </a>
        </div>
    </h2>
    <small>Listagem geral de médicos cadastrados no sistema</small>
@stop

@section('content')
<div class="row clearfix">

    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
        <div class="search-filters"
                style="
                    background-color: #f4f4f4;
                    margin-bottom: 12px;
                    border: 1px solid rgba(0,0,0,.15);
                    border-radius: .25rem;
                "
            >
                <form class="form-inline" method="GET">
                    <label class="mr-sm-2" for="hospital" style="margin-left:12px;">Hospital</label>
                    <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="hospital" name="hospital">
                        <option selected></option>
                        @foreach( $hospitals as $hospital)
                            <option value="{{ $hospital->id }}" @if( Request::input('hospital') == $hospital->id ) selected @endif>{{ $hospital->name }}</option>
                        @endforeach
                    </select>
                    

                    <label class="mr-sm-2" for="email" style="margin-left:12px;">Email</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        id="email" 
                        name="email"
                        placeholder="Email"
                        style="
                            margin-top: 6px;
                            margin-right: 12px;
                            margin-left: 12px;
                            background-size: 8px 10px;
                            border: 1px solid rgba(0,0,0,.15);
                            border-radius: .25rem;
                            padding: 6px;
                            display: inline-block;
                            max-width: 100%;
                            min-width: 40%;
                            height: calc(2.25rem + 2px);
                        "
                        value="{{ Request::input('email') }}"
                    />
                    <label class="mr-sm-2" for="name" style="margin-left:12px;">Nome</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        id="name" 
                        name="name"
                        placeholder="Nome do Médico"
                        style="
                            margin-top: 6px;
                            margin-right: 12px;
                            margin-left: 12px;
                            background-size: 8px 10px;
                            border: 1px solid rgba(0,0,0,.15);
                            border-radius: .25rem;
                            padding: 6px;
                            display: inline-block;
                            min-width: 45%;
                            max-width: 100%;
                            height: calc(2.25rem + 2px);
                        "
                        value="{{ Request::input('name') }}"
                    />
                    
                    <button type="submit" class="btn btn-raised btn-primary">Filtrar</button>
                </form>
            </div>
        </div>
    
    @foreach($doctors as $doctor)
        <div class="col-xl-3 col-lg-4 col-md-6 col-sm-12">
            <div class="card">
                <div class="body">
                    <div class="member-card verified">                            
                        <div class="thumb-xl member-thumb">
                            @if( ($doctor->user) && ($doctor->user->photo ) )
                                <img src="/profile_photo/{{ $doctor->user->photo }}" class="img-thumbnail rounded-circle" alt="profile-image">
                            @else
                                <img src="/vendor/swift/assets/images/random-avatar3.jpg" class="img-thumbnail rounded-circle" alt="profile-image"> 
                            @endif                            
                        </div>

                        <div class="">
                            <h4 class="m-b-5 m-t-20">{{ $doctor->name }}</h4>
                            <p class="text-muted">{{ $doctor->speciality }}</p>
                        </div>

                        <a href="{{ route('doctors.edit', ['id' => $doctor->id ]) }}"  class="btn btn-raised btn-sm btn-primary">Atualizar</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
        {!! $doctors->links("pagination::bootstrap-4") !!}
    </div>
</div>
@stop

@section('js')
    <script src="{{ asset('js/doctors.js') }}"></script>
@stop
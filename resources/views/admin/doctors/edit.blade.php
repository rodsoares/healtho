@extends('layouts.base')

@section('content')
    <form action="{{ route('doctors.update', ['id' => $doctor->id]) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <div class="card">
            <div class="header">
                <h2>Informações Básicas</h2>
                <small>Digita as informações referentes ao médico</small>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="name">Nome completo</label>
                                <input id="name" type="text" class="form-control" name="name" placeholder="Nome Completo" value="{{ $doctor->name }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="speciality">Especialidade</label>
                                <input type="text" class="form-control" name="speciality" id="speciality" value="{{ $doctor->speciality }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group drop-custum focused">
                            <label for="hospital_id">Hospital</label>
                            <select class="form-control show-tick" name="hospital_id">
                                <option value="">- Hospital -</option>
                                @foreach ($hospitals as $item)
                                    <option value="{{ $item->id }}" @if( $item->id === $doctor->hospital_id) selected @endif>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="mother">Nome da Mãe</label>
                                <input type="text" class="form-control" name="mother" id="mother" value="{{ $doctor->mother }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="pis-nit">PIS/NIT</label>
                                <input type="text" class="form-control"  name="pis_nit" id="pis-nit" value="{{ $doctor->pis_nit }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="crm">RG</label>
                                <input type="text" class="form-control"  name="rg" id="rg" value="{{ $doctor->rg }}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="marital_status">Estado Civil</label>
                                <input id="marital_status" type="text" class="form-control" name="marital_status" value="{{ $doctor->marital_status }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="nationality">Naturalidade</label>
                                <input type="text" class="form-control" name="nationality" id="nationality" value="{{ $doctor->nationality }}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="cpf">CPF</label>
                                <input type="text" class="form-control" placeholder="CPF" name="cpf" id="cpf"
                                       data-mask="000.000.000-00" value="{{ $doctor->cpf }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="crm">CRM</label>
                                <input type="text" class="form-control" placeholder="CRM" name="crm" id="crm" value="{{ $doctor->crm }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="started_in">Começou em:</label>
                                <input type="text" class="form-control" placeholder="00/00/0000"
                                       name="started_in" data-mask="00/00/0000" id="started_in" value="{{ $doctor->started_in }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="cellphone_number">Celular</label>
                                <input type="text" class="form-control" placeholder="Celular (xx) x xxxx-xxxx"
                                    name="cellphone_number" data-mask="(00) 0 0000-0000" id="cellphone_number" value="{{ $doctor->cellphone_number }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" placeholder="Entre com o email" name="email" id="email" value="{{ $doctor->email }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="address">Endereço</label>
                                <input type="text" class="form-control" placeholder="Endereço" name="address" id="address" value="{{ $doctor->address }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="header">
                <h2>Dados Bancários</h2>
                <small>Informações bancárias do Médico</small>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="bank">Banco</label>
                                <input type="text" class="form-control" name="bank" id="bank" value="{{ $doctor->bank }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="bank_branch">Agência</label>
                                <input type="text" class="form-control" name="bank_branch" id="bank_branch" value="{{ $doctor->bank_branch }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="checking_account">Conta Corrente</label>
                                <input type="text" class="form-control" name="checking_account" id="checking_account" value="{{ $doctor->checking_account }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <div class="card">
                <div class="header">
                    <h2>Informações de Usuário</h2>
                    <small>Defina a senha</small>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group">
                            <div class="form-line">
                                <label for="password">Senha</label>
                                <input type="password" class="form-control" name="password" id="password">
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="password_confirmation">Confirme a senha</label>
                                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="float-right">
            <button type="submit" class="btn btn-success btn-raised">
                Atualizar Médico
            </button>
            <a role="button" class="btn btn-danger btn-raised"
                href="{{ route('doctors.index') }}">Cancelar</a>
        </div>
    </form>

    <div class="card">
        <div class="header">
            <h2>Documentos Requisitados</h2>
        </div>
        <div class="body">
            <div class="table-responsive">
                <table class="table table-sm table-bordered table-striped">
                    <thead class="bg-cyan">
                        <td>ID</td>
                        <td>Nome</td>
                        <td></td>
                    </thead>
                    <tbody>
                        @foreach( $doctor->files as $file )
                            @if( $file->category === 'DOCTOR')
                                <tr>
                                    <td>{{ $file->id }}</td>
                                    <td>{{ $file->name }}</td>
                                    <td class="text-right">
                                        <a role="button" target="_blank" class="btn btn-primary btn-xs btn-raised" href="/admin/files/{{$file->id}}/download" title="download" style="margin-right: 3px"><i class="fa fa-fw fa-download"></i> download</a>
                                        <button type="button" class="btn btn-danger btn-xs btn-raised" onclick="deleteFile({{$file->id}})"><i class="fa fa-fw fa-trash" title="apagar"></i> apagar</button>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="header">
            <h2>Documentos</h2>
        </div>
        <div class="body">
            <div class="table-responsive">
                <table class="table table-sm table-bordered table-striped">
                    <thead class="bg-cyan">
                    <td>ID</td>
                    <td>Nome</td>
                    <td>Tipo</td>
                    <td>Médico</td>
                    <td></td>
                    </thead>
                    <tbody>
                    @foreach( $doctor->files as $file )
                        @if( $file->category != 'DOCTOR')
                            <tr>
                                <td>{{ $file->id }}</td>
                                <td>{{ $file->name }}</td>
                                <td>{{ $file->category }}</td>
                                <td>{{ $file->owner->name }}</td>
                                <td class="text-right">
                                    <a role="button" target="_blank" class="btn btn-primary btn-xs btn-raised" href="/admin/files/{{$file->id}}/download" title="download" style="margin-right: 3px"><i class="fa fa-fw fa-download"></i> download</a>
                                    <button type="button" class="btn btn-danger btn-xs btn-raised" onclick="deleteFile({{$file->id}})"><i class="fa fa-fw fa-trash" title="apagar"></i> apagar</button>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <form id="delete_form" method="post">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
@stop

@section('js')
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"
            integrity="sha256-PtzTX1ftmEmj8YUiAX0wTIQ+ddTAGVt2MiLMsGsAMxM="
            crossorigin="anonymous"
    ></script>
    <script src="{{ asset('js/files.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.select2').select2();
        })
    </script>
@stop

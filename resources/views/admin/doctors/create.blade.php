@extends('layouts.base')

@section('block-header')
    <h2>Adicionar novo Médico</h2>
    <small>Fomulário de Cadastrado</small>
@stop

@section('content')
    <form action="{{ route('doctors.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="card">
            <div class="header">
                <h2>Informações Básicas</h2>
                <small>Digita as informações referentes ao médico</small>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="name">Nome completo</label>
                                <input id="name" type="text" class="form-control" name="name" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="speciality">Especialidade</label>
                                <input type="text" class="form-control" name="speciality" id="speciality" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group drop-custum focused">
                            <label for="hospital_id">Hospital</label>
                            <select class="form-control show-tick custom-select" name="hospital_id" required>
                                @foreach ($hospitals as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="mother">Nome da Mãe</label>
                                <input type="text" class="form-control" name="mother" id="mother" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="pis-nit">PIS/NIT</label>
                                <input type="text" class="form-control" name="pis_nit" id="pis-nit" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="crm">RG</label>
                                <input type="text" class="form-control" name="rg" id="rg" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="marital_status">Estado Civil</label>
                                <input id="marital_status" type="text" class="form-control" name="marital_status" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="nationality">Naturalidade</label>
                                <input type="text" class="form-control" name="nationality" id="nationality" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="cpf">CPF</label>
                                <input type="text" class="form-control" placeholder="CPF" name="cpf" id="cpf"
                                       data-mask="000.000.000-00" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="crm">CRM</label>
                                <input type="text" class="form-control" placeholder="CRM" name="crm" id="crm" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="started_in">Iniciou em:</label>
                                <input type="text" class="form-control" placeholder="xx/xx/xxxx"
                                       name="started_in" data-mask="00/00/0000" id="started_in" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="cellphone_number">Celular</label>
                                <input type="text" class="form-control" placeholder="Celular (xx) x xxxx-xxxx"
                                    name="cellphone_number" data-mask="(00) 0 0000-0000" id="cellphone_number" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" placeholder="Entre com o email" name="email" id="email" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="address">Endereço</label>
                                <input type="text" class="form-control" placeholder="Endereço" name="address" id="address" required>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <div class="card">
                <div class="header">
                    <h2>Dados Bancários</h2>
                    <small>Informações bancárias do Médico</small>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="bank">Banco</label>
                                    <input type="text" class="form-control" name="bank" id="bank" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="bank_branch">Agência</label>
                                    <input type="text" class="form-control" name="bank_branch" id="bank_branch" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="checking_account">Conta Corrente</label>
                                    <input type="text" class="form-control" name="checking_account" id="checking_account" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="header">
                    <h2>Informações de Usuário</h2>
                    <small>Defina a senha</small>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <div class="col-sm-6">
                            <div class="form-group">
                            <div class="form-line">
                                <label for="password">Senha</label>
                                <input type="password" class="form-control" name="password" id="password" required>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="form-line">
                                    <label for="password_confirmation">Confirme a senha</label>
                                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="float-right">
                        <button type="submit" class="btn btn-success btn-raised">
                            Adicionar Médico
                        </button>
                        <a role="button" class="btn btn-danger btn-raised"
                            href="{{ route('doctors.index') }}">Cancelar</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
@stop

@section('js')
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"
            integrity="sha256-PtzTX1ftmEmj8YUiAX0wTIQ+ddTAGVt2MiLMsGsAMxM="
            crossorigin="anonymous"
    ></script>
    <script>
        $(document).ready(function(){
            $('.custom-selectyy').select2({
                theme: "bootstrap4"
            });
        })
    </script>
@stop
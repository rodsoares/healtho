@extends('layouts.base')


@section('block-header')
    <h2>
        Dashboard Administrivo
        <small>Hospitais</small>
    </h2>
@stop

@section('content')
    <div class="card card-default">
        <div class="header">
            <h2>
                <i class="fa fa-fw fa-building-o"></i>
                Hospitais cadastrados no sistema
                <a role="button" 
                    class="btn btn-raised btn-primary btn-sm float-right" 
                    style="margin-top: -1px"
                    href="{{ route('hospitals.create') }}">Adicionar Hospital</a>
            </h2>
        </div>
        <div class="body">
            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <thead>
                        <th>ID</td>
                        <th>Status</th>
                        <th>Nome</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach( $hospitals as $hospital )
                            <tr>
                                <td>{{ $hospital->id }}</td>
                                <td>{{ $hospital->status ? 'Ativo' : 'Inativo'  }}</td>
                                <td>{{ $hospital->name }}</td>
                                <td class="text-right">
                                    <a role="button" 
                                        class="btn btn-raised btn-default btn-circle waves-effect waves-circle wave-float"
                                        href="{{ route('hospitals.edit', ['id' => $hospital->id]) }}">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <button type="button" 
                                        class="btn btn-raised btn-danger btn-circle waves-effect waves-circle wave-float"
                                        onclick="deleteHospital({{$hospital->id}})">
                                        <i class="material-icons">delete</i>
                                    </button>
                                    <form id="hospital-{{ $hospital->id }}" action="{{ route('hospitals.destroy', ['id' => $hospital->id]) }}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $hospitals->links("pagination::bootstrap-4") !!}
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('js/hospitals.js') }}?{{ date('dmYHis') }}"></script>
@stop
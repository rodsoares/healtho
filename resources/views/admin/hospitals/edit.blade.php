@extends('layouts.base')

@section('title-header')
    <h2>
        Dashboard Administrivo
        <small>Hospitais</small>
    </h2>
@stop

@section('content')
    <div class="card">
        <div class="header">
            <h2>Formulário de Edição de Hospitais</h2>
        </div>
        <div class="body">
            <form action="{{ route('hospitals.update', ['id' => $hospital->id ]) }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

                <input type="hidden" name="status" value="1">
                <div class="form-group row">
                    <div class="form-line">
                        <label for="name">Nome do Hospital</label>
                        <input type="text" class="form-control" id="name" value="{{ $hospital->name }}" name="name">
                    </div>
                </div>

                <button type="submit" class="btn btn-raised">Atualizar Registro</button>
                <a role="button" class="btn btn-raised btn-danger " href="{{ route('hospitals.index') }}">Cancelar</a>
            </form>
        </div>
    </div>
@stop
@extends('layouts.base')

@section('title-header')
    <h2>
        Dashboard Administrivo
        <small>Usuários</small>
    </h2>
@stop

@section('content')
    <div class="card">
        <div class="header">
            <h2>Formulário de Cadastro de Usuário</h2>
        </div>
        <div class="body">
            <form action="{{ route('users.update', ['id' => $user->id]) }}" method="POST">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <input type="hidden" name="status" value="1">

                <div class="form-group row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="name">Nome</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" value="{{ $user->email }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group drop-custum focused">
                            <label for="role_id">Categoria</label>
                            <select class="form-control show-tick" name="role_id">
                                @foreach ($roles as $item)
                                    @if( $item->name != 'doctor')
                                        <option value="{{ $item->id }}" selected>{{ $item->name == 'admin' ? 'Administrador':'Médico' }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="password">Senha</label>
                                <input type="password" class="form-control" id="password" name="password">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="password_confirmation">Confirme a senha</label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-raised">Atualizar Registro</button>
                <a role="button" class="btn btn-raised btn-danger " href="{{ route('users.index') }}">Cancelar</a>
            </form>
        </div>
    </div>
@stop
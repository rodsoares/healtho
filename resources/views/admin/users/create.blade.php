@extends('layouts.base')

@section('title-header')
    <h2>
        Dashboard Administrivo
        <small>Usuários</small>
    </h2>
@stop

@section('content')
    <div class="card">
        <div class="header">
            <h2>Formulário de Cadastro de Usuário</h2>
        </div>
        <div class="body">
            <form action="{{ route('users.store') }}" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="status" value="1">

                <div class="form-group row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="name">Nome</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group drop-custum focused">
                            <label for="role_id">Categoria</label>
                            <select class="form-control show-tick" name="role_id" required>
                                @foreach ($roles as $item)
                                    @if( $item->name != 'doctor')
                                        <option value="{{ $item->id }}">{{ $item->name == 'admin' ? 'Administrador':'Médico' }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="password">Senha</label>
                                <input type="password" class="form-control" id="password" name="password" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="password_confirmation">Confirme a senha</label>
                                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" required>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-raised">Adicionar Registro</button>
                <a role="button" class="btn btn-raised btn-danger " href="{{ route('users.index') }}">Cancelar</a>
            </form>
        </div>
    </div>
@stop
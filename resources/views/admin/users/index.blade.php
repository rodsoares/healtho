@extends('layouts.base')


@section('block-header')
    <h2>
        Dashboard Administrivo
        <small>Hospitais</small>
    </h2>
@stop

@section('content')
    <div class="card card-default">
        <div class="header">
            <h2>
                <i class="fa fa-fw fa-building-o"></i>
                Usuários cadastrados no sistema
                <a role="button" 
                    class="btn btn-raised btn-primary btn-sm float-right" 
                    style="margin-top: -1px"
                    href="{{ route('users.create') }}">Adicionar Usuário</a>
            </h2>
        </div>
        <div class="body">

        <div class="search-filters"
                style="
                    background-color: #f4f4f4;
                    margin-bottom: 12px;
                    border: 1px solid rgba(0,0,0,.15);
                    border-radius: .25rem;
                "
            >
                <form class="form-inline" method="GET">
                    <label class="mr-sm-2" for="role" style="margin-left:12px;">Categoria</label>
                    <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="role" name="role">
                        <option selected></option>
                        <option value="doctor">MÉDICO</option>
                        <option value="admin">ADMINISTRADOR</option>
                    </select>
                    

                    <label class="mr-sm-2" for="email" style="margin-left:12px;">Email</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        id="email" 
                        name="email"
                        placeholder="Título"
                        style="
                            margin-top: 6px;
                            margin-right: 12px;
                            margin-left: 12px;
                            background-size: 8px 10px;
                            border: 1px solid rgba(0,0,0,.15);
                            border-radius: .25rem;
                            padding: 6px;
                            display: inline-block;
                            max-width: 100%;
                            min-width: 23%;
                            height: calc(2.25rem + 2px);
                        "
                    />
                    <label class="mr-sm-2" for="name" style="margin-left:12px;">Nome</label>
                    <input 
                        type="text" 
                        class="form-control" 
                        id="name" 
                        name="name"
                        placeholder="Nome do Usuário"
                        style="
                            margin-top: 6px;
                            margin-right: 12px;
                            margin-left: 12px;
                            background-size: 8px 10px;
                            border: 1px solid rgba(0,0,0,.15);
                            border-radius: .25rem;
                            padding: 6px;
                            display: inline-block;
                            min-width: 23%;
                            max-width: 100%;
                            height: calc(2.25rem + 2px);
                        "
                    />
                    
                    <button type="submit" class="btn btn-raised btn-primary">Filtrar</button>
                </form>
            </div>

            <div class="table-responsive">
                <table class="table table-sm table-striped">
                    <thead>
                        <th>ID</td>
                        <th>Nome</th>
                        <th>Email</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach( $users as $user )
                            @if( ($user->id != Auth::user()->id) )
                                <tr>
                                    <td>{{ $user->id }}</td>
                                    <td>
                                        @if( count($user->roles) )
                                            @if( $user->roles[0]->name == 'admin')
                                                <span class="label label-primary">administrador</span>
                                            @else
                                                <span class="label label-info">médico</span>
                                            @endif
                                            &nbsp;
                                        @else
                                            <span class="label label-danger">sem acesso</span>
                                            &nbsp;
                                        @endif

                                        {{ $user->name }}
                                    </td>
                                    <td>{{ $user->email }}</td>
                                    <td class="text-right">
                                        
                                        @if ($user->roles[0]->name != 'doctor')
                                            <a role="button"
                                                class="btn btn-raised btn-default btn-circle waves-effect waves-circle wave-float"
                                                href="{{ route('users.edit', ['id' => $user->id]) }}">
                                                <i class="material-icons">edit</i>
                                            </a>
                                        @endif
                                        <button type="button"
                                            class="btn btn-raised btn-danger btn-circle waves-effect waves-circle wave-float"
                                            onclick="deleteUser({{$user->id}})">
                                            <i class="material-icons">delete</i>
                                        </button>
                                        <form id="user-{{ $user->id }}" action="{{ route('users.destroy', ['id' => $user->id]) }}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
                {!! $users->links("pagination::bootstrap-4") !!}
            </div>
        </div>
    </div>
@stop

@section('js')
    <script src="{{ asset('js/users.js') }}?{{ date('dmYHis') }}"></script>
@stop
@extends('layouts.base')

@section('title-header')
    <h2>
        Dashboard Administrivo
        <small>Indicadores de Produtividade</small>
    </h2>
@stop

@section('content')
    <div class="card">
        <div class="header">
            <h2>Formulário de Cadastro de Indicadores</h2>
        </div>
        <div class="body">
            <form action="{{ route('indicators.store') }}" method="POST">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="year">Ano</label>
                                <input type="number" class="form-control" id="year" name="year" min="{{ date('Y') - 1 }}" max="{{ date('Y') }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="month">Mês</label>
                                <input type="number" min="1" max="12" class="form-control" id="month" name="month" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="work_shift">Turnos Trabalhados</label>
                                <input type="text" class="form-control" id="work_shift" name="work_shift" min="0" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="value_per_shift">Valor por Hora Trabalhada</label>
                                <input type="text" class="form-control" id="value_per_shift" name="value_per_shift" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group drop-custum focused">
                            <label for="doctor_id">Médico</label>
                            <select class="form-control show-tick" name="doctor_id" required>
                                @foreach ($doctors as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-raised">Adicionar Registro</button>
                <a role="button" class="btn btn-raised btn-danger " href="{{ back() }}">Cancelar</a>
            </form>
        </div>
    </div>
@stop
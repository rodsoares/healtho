@extends('layouts.base')


@section('content')
    <div class="card card-default">
        <div class="header">
            <h2>
                Indicadores de Produtividade
                <a role="button" class="btn btn-sm btn-raised btn-primary float-right" href="{{ route('indicators.create') }}">
                    Adicionar Novo Indicador
                </a>
                &nbsp;
                <button role="button" class="btn btn-sm btn-raised btn-secondary float-right">
                    Importar Arquivo
                </button>
            </h2>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-bordered table-striped">
                    <thead>
                        <th>ID</th>
                        <th>Data</th>
                        <th>Turnos</th>
                        <th>Valor por Turno</th>
                        <th>Médico</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach($indicators as $indicator)
                            <tr>
                                <td>{{ $indicator->id }}</td>
                                <td>{{ $indicator->month }}/{{ $indicator->year }}</td>
                                <td>{{ $indicator->work_shift }}</td>
                                <td>{{ $indicator->value_per_shift }}</td>
                                <td>{{ $indicator->doctor->name }}</td>
                                <td class="text-right">
                                    <button type="button" class="btn btn-danger btn-xs btn-raised" onclick="deleteIndicator({{$indicator->id}})">apagar</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <form id="delete_form" method="post">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
@stop

@section('js')
    <script>
        function deleteIndicator( id ) {
            swal({
                title: "Você tem certeza?",
                text: "Após essa ação o registro será deletado permanentemente?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then(willDelete => {
                    if ( willDelete ) {
                        $('#delete_form').attr('action', '/admin/indicators/'+id);
                        $('#delete_form').submit();
                    }
                })
        }
    </script>
@stop
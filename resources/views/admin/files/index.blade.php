@extends('layouts.base')


@section('content')
    <div class="card card-default">
        <div class="header">
            <h2>
                Arquivos Adicionados
                <a role="button" class="btn btn-sm btn-raised btn-primary float-right" href="{{ route('files.create') }}">
                    Adicionar Novo Arquivo
                </a>
            </h2>
        </div>
        <div class="card-body">
            <div class="search-filters"
                style="
                    background-color: #f4f4f4;
                    margin-bottom: 12px;
                    border: 1px solid rgba(0,0,0,.15);
                    border-radius: .25rem;
                "
            >
                <form class="form-inline" method="GET">
                    <label class="mr-sm-2" for="document_category" style="margin-left:12px;">Tipo Documento</label>
                    <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="document_category" name="document_category">
                        <option selected></option>
                        <option value="DOC" @if( Request::input('document_category') == 'DOC' ) selected @endif>DOC</option>
                        <option value="EXCEL" @if( Request::input('document_category') == 'EXCEL' ) selected @endif>EXCEL</option>
                        <option value="PDF" @if( Request::input('document_category') == 'PDF' ) selected @endif>PDF</option>
                        <option value="NFE" @if( Request::input('document_category') == 'NFE' ) selected @endif>NFE</option>
                    </select>

                    <label class="mr-sm-2" for="doctor" style="margin-left:12px;">Médico</label>
                    <select class="custom-select mb-2 mr-sm-2 mb-sm-0" id="doctor" name="doctor">
                        <option selected></option>
                        @foreach( $doctors as $doctor)
                            <option value="{{ $doctor->id }}" @if( Request::input('doctor') == $doctor->id ) selected @endif>{{ $doctor->name }}</option>
                        @endforeach
                    </select>
                    
                    <input 
                        type="text" 
                        class="form-control" 
                        id="document_name" 
                        name="document_name"
                        placeholder="Título"
                        style="
                            margin-top: 6px;
                            margin-right: 12px;
                            margin-left: 12px;
                            background-size: 8px 10px;
                            border: 1px solid rgba(0,0,0,.15);
                            border-radius: .25rem;
                            padding: 6px;
                            display: inline-block;
                            max-width: 100%;
                            height: calc(2.25rem + 2px);
                        "
                        value="{{ Request::input('document_name') }}"
                    />
                    
                    <button type="submit" class="btn btn-raised btn-primary">Filtrar</button>
                </form>
            </div>

            <div class="table-responsive">
                <table class="table table-sm table-bordered table-striped">
                    <thead>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Tipo</th>
                        <th>Médico</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach($files as $file)
                            <tr>
                                <td>{{ $file->id }}</td>
                                <td>{{ $file->name }}</td>
                                <td>{{ $file->category }}</td>
                                <td>{{  $file->owner ? $file->owner->name : 'não informado' }}</td>
                                <td class="text-right">
                                    <a role="button" target="_blank" class="btn btn-primary btn-xs btn-raised" href="/admin/files/{{$file->id}}/download" title="download">download</a>
                                    <button type="button" class="btn btn-danger btn-xs btn-raised" onclick="deleteFile({{$file->id}})">apagar</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            
            {!! $files->links("pagination::bootstrap-4") !!}
        </div>
    </div>

    <form id="delete_form" method="post">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
@stop

@section('js')
    <script src="{{ asset('js/files.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.custom-select').select2({
                theme: "bootstrap4"
            });
        });
    </script>
@stop
@extends('layouts.base')

@section('content')
    <div class="card">
        <div class="header">
            <h2>
                <i class="fa fa-fw fa-file-excel-o"></i>
                Formulário de adição de novo arquivo
            </h2>
        </div>
        <div class="body">
            <form action="{{ route('files.store') }}" enctype="multipart/form-data" method="POST">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="name" >Nome do Arquivo</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="arquivo.pdf">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group drop-custum focused">
                            <label for="category" >Tipo</label>
                            <select class="form-control" id="category" name="category">
                                <option>Selecione o tipo</option>
                                <option value="DOC">DOC</option>
                                <option value="EXCEL">EXCEL</option>
                                <option value="PDF">PDF</option>
                                <option value="NFE">NFE</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group drop-custum focused">
                            <label for="owner">Médico</label>
                            <select class="form-control" id="owner" name="owner" required>
                                <option>Selecione o Médico</option>
                                @foreach( $doctors as $doctor)
                                    <option value="{{ $doctor->id }}">{{ $doctor->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-lg-12 col-md-12">
                        <div class="form-group float-right">
                            <input type="file" class="form-control-file" id="file" name="file">
                        </div>
                    </div>
                </div>

                <div class="float-right">
                    <button type="submit" class="btn btn-primary btn-raised">
                        Upload
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script 
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js" 
        integrity="sha256-PtzTX1ftmEmj8YUiAX0wTIQ+ddTAGVt2MiLMsGsAMxM=" 
        crossorigin="anonymous"
    ></script>
    <script src="/js/bootstrap-fileselect.js"></script>
    <script>
        $('#file').fileselect({
            language: "en",
            browseBtnClass: 'btn btn-danger btn-raised'
        });
    </script>
@stop
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title>:: Sistema - Martins&Gagliotti ::</title>
<link href="/vendor/swift/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- Custom Css -->
<link href="/vendor/swift/assets/css/main.css" rel="stylesheet">
<link href="/vendor/swift/assets/css/login.css" rel="stylesheet">

<!-- Swift Themes. You can choose a theme from css/themes instead of get all themes -->
<link href="/vendor/swift/assets/css/themes/all-themes.css" rel="stylesheet" />
</head>
<body class="theme-cyan login-page authentication">

<div class="container">
    <div class="card-top"></div>
    <div class="card">
        <h1 class="title"><span>Martins & Gagliotti</span>Login <span class="msg">Insira suas credenciais para entrar no sistema</span></h1>
        <div class="col-md-12">
            <form id="sign_in" action="/login" method="POST">
                {{ csrf_field() }}                
                <div class="input-group"> <span class="input-group-addon"> <i class="zmdi zmdi-account"></i> </span>
                    <div class="form-line">
                        <input type="email" class="form-control" name="email" placeholder="email" required autofocus>
                    </div>
                </div>
                <div class="input-group"> <span class="input-group-addon"> <i class="zmdi zmdi-lock"></i> </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" placeholder="Senha" required>
                    </div>
                </div>
                <div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-raised btn-block waves-effect g-bg-cyan">ENTRAR</button>
                    </div>
                </div>
            </form>
        </div>
    </div>    
</div>
<!--div class="theme-bg"></div-->

<!-- Jquery Core Js --> 
<script src="/vendor/swift/assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="/vendor/swift/assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<script src="/vendor/swift/assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
</body>
</html>

﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title>:: Sistema - Martins&Gagliotti ::</title>
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link href="/vendor/swift/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <link href="/vendor/swift/assets/css/main.css" rel="stylesheet">
        <!-- Custom Css -->

        <link href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" />

        <!-- Swift Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="/vendor/swift/assets/css/themes/all-themes.css" rel="stylesheet" />

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <link href="/css/select2-bootstrap4.min.css" rel="stylesheet" />
    </head>

    <body class="theme-cyan">

        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Aguarde uns instantes...</p>
            </div>
        </div>
        <!-- #END# Page Loader --> 

        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->

        <!-- Top Bar -->
        <nav class="navbar clearHeader">
            <div class="col-12">
                <div class="navbar-header"> <a href="javascript:void(0);" class="bars"></a> <a class="navbar-brand" href="/">Martins & Gagliotti</a> </div>
                <ul class="nav navbar-nav navbar-right">
                    <!-- Notifications -->
                    <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button"><i class="zmdi zmdi-notifications"></i> <span class="label-count">7</span> </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu">
                                    <li> <a href="javascript:void(0);">
                                        <div class="icon-circle bg-light-green"><i class="zmdi zmdi-account-add"></i></div>
                                        <div class="menu-info">
                                            <h4>12 new members joined</h4>
                                            <p> <i class="material-icons">access_time</i> 14 mins ago </p>
                                        </div>
                                        </a> 
                                    </li>
                                    <li> <a href="javascript:void(0);">
                                        <div class="icon-circle bg-cyan"><i class="zmdi zmdi-shopping-cart-plus"></i></div>
                                        <div class="menu-info">
                                            <h4>4 sales made</h4>
                                            <p> <i class="material-icons">access_time</i> 22 mins ago </p>
                                        </div>
                                        </a> 
                                    </li>
                                    <li> <a href="javascript:void(0);">
                                        <div class="icon-circle bg-red"><i class="zmdi zmdi-delete"></i></div>
                                        <div class="menu-info">
                                            <h4><b>Nancy Doe</b> deleted account</h4>
                                            <p> <i class="material-icons">access_time</i> 3 hours ago </p>
                                        </div>
                                        </a> 
                                    </li>
                                    <li> <a href="javascript:void(0);">
                                        <div class="icon-circle bg-orange"><i class="zmdi zmdi-edit"></i></div>
                                        <div class="menu-info">
                                            <h4><b>Nancy</b> changed name</h4>
                                            <p> <i class="material-icons">access_time</i> 2 hours ago </p>
                                        </div>
                                        </a> 
                                    </li>
                                    <li> <a href="javascript:void(0);">
                                        <div class="icon-circle bg-blue-grey"><i class="zmdi zmdi-comment-alt-text"></i></div>
                                        <div class="menu-info">
                                            <h4><b>John</b> commented your post</h4>
                                            <p> <i class="material-icons">access_time</i> 4 hours ago </p>
                                        </div>
                                        </a> 
                                    </li>
                                    <li> <a href="javascript:void(0);">
                                        <div class="icon-circle bg-light-green"><i class="zmdi zmdi-refresh-alt"></i></div>
                                        <div class="menu-info">
                                            <h4><b>John</b> updated status</h4>
                                            <p> <i class="material-icons">access_time</i> 3 hours ago </p>
                                        </div>
                                        </a> 
                                    </li>
                                    <li> <a href="javascript:void(0);">
                                        <div class="icon-circle bg-purple"><i class="zmdi zmdi-settings"></i></div>
                                        <div class="menu-info">
                                            <h4>Settings updated</h4>
                                            <p> <i class="material-icons">access_time</i> Yesterday </p>
                                        </div>
                                        </a> 
                                    </li>
                                </ul>
                            </li>
                            <li class="footer"> <a href="javascript:void(0);">View All Notifications</a> </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications --> 

                    <!-- Tasks -->
                    <li class="dropdown"> 
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="zmdi zmdi-flag"></i><span class="label-count">9</span> 
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">TASKS</li>
                            <li class="body">
                                <ul class="menu tasks">
                                    <li> <a href="javascript:void(0);">
                                        <h4> Task 1 <small>32%</small> </h4>
                                        <div class="progress">
                                            <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%"> </div>
                                        </div>
                                        </a> 
                                    </li>
                                    <li> <a href="javascript:void(0);">
                                        <h4>Task 2 <small>45%</small> </h4>
                                        <div class="progress">
                                            <div class="progress-bar bg-cyan" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 45%"> </div>
                                        </div>
                                        </a> 
                                    </li>
                                    <li> <a href="javascript:void(0);">
                                        <h4>Task 3 <small>54%</small> </h4>
                                        <div class="progress">
                                            <div class="progress-bar bg-teal" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 54%"> </div>
                                        </div>
                                        </a> 
                                    </li>
                                    <li> <a href="javascript:void(0);">
                                        <h4> Task 4 <small>65%</small> </h4>
                                        <div class="progress">
                                            <div class="progress-bar bg-orange" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 65%"> </div>
                                        </div>
                                        </a> 
                                    </li>                          
                                </ul>
                            </li>
                            <li class="footer"> <a href="javascript:void(0);">View All Tasks</a> </li>
                        </ul>
                    </li>
                    <!-- #END# Tasks -->

                    <li>
                        <a href="javascript:void(0);" class="js-right-sidebar" data-close="true">
                            <i class="zmdi zmdi-settings"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        <!-- #Top Bar -->

        <section> 
            <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar"> 
                <!-- User Info -->
                <div class="user-info">
                    <div class="admin-image">
                        @if ( Auth::user()->photo )
                            <img src="/profile_photo/{{ Auth::user()->photo }}" alt="">
                        @else
                            <img src="/vendor/swift/assets/images/random-avatar7.jpg" alt="">
                        @endif
                    </div>
                    <div class="admin-action-info"> <span>Bem vindo</span>
                        <h3>{{ Auth::user()->doctor ? explode(' ', Auth::user()->doctor->name)[0] : explode(' ', Auth::user()->name)[0] }}</h3>
                        <ul>
                            <li><a data-placement="bottom" title="Perfil" href="{{ route('profile') }}"><i class="zmdi zmdi-account"></i></a></li>
                            <li><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="zmdi zmdi-settings"></i></a></li>
                            <li>
                                <form action="/logout" method="post" id="logout">{{ csrf_field() }}</form>
                                <a role="button" onclick="javascript:$('#logout').submit()"
                                   data-placement="bottom" title="Full Screen">
                                    <i class="zmdi zmdi-sign-in"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="quick-stats">
                        <h5>Dados Gerais</h5>
                        @is('doctor')
                            <ul>
                                <li><span id="total"><i>Total</i></span></li>
                                <li><span id="nfe"><i>NFE</i></span></li>
                                <li><span id="demais"><i>Demais</i></span></li>
                            </ul>
                        @endis

                        @is('admin')
                            <ul>
                                <li><span id="hospitals"><i>Total</i></span></li>
                                <li><span id="doctors"><i>Documentos</i></span></li>
                                <li><span id="documents"><i>NFE</i></span></li>
                            </ul>
                        @endis
                    </div>
                </div>
                <!-- #User Info --> 
                <!-- Menu -->
                <div class="menu">
                    <ul class="list">
                        <li class="header">Menu Principal</li>
                        <li><a href="/home"><i class="zmdi zmdi-home"></i><span>Dashboard</span></a></li>  
                        
                        @is('admin')
                        <li><a href="{{ route('indicators.index') }}"><i class="zmdi zmdi-brightness-medium"></i><span>Indicadores</span></a></li>
                        <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-calendar-check"></i><span>Arquivos</span> </a>
                            <ul class="ml-menu">
                                <li><a href="{{ route('files.index') }}">Listagem Geral</a></li>
                                <li><a href="{{ route('files.create') }}">Adicionar Arquivo</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account-add"></i><span>Médicos</span> </a>
                            <ul class="ml-menu">
                                <li><a href="{{ route('doctors.index') }}">Listagem Geral</a></li>
                                <li><a href="{{ route('doctors.create') }}">Adicionar Médico</a></li>
                            </ul>
                        </li>
                        <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-developer-board"></i><span>Hospitais</span> </a>
                            <ul class="ml-menu">
                                <li><a href="{{ route('hospitals.index') }}">Listagem Geral</a></li>
                                <li><a href="{{ route('hospitals.create') }}">Adicionar Hospital</a></li>                       
                            </ul>
                        </li>
                        <li><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-account-o"></i><span>Usuários</span> </a>
                            <ul class="ml-menu">
                                <li><a href="{{ route('users.index') }}">Listagem Geral</a></li>
                                <li><a href="{{ route('users.create') }}">Adicionar Usuário Administrativo</a></li>
                            </ul>
                        </li>
                        @endis

                        @is('doctor')
                            <li><a href="/home/?type=all"><i class="zmdi zmdi-chart"></i><span>Resultados</span></a></li>
                            <li><a href="{{ route('doctor.files.index') }}"><i class="zmdi zmdi-assignment"></i><span>Documentos</span></a></li>
                            <li><a href="/home/?type=nfe"><i class="zmdi zmdi-card"></i><span>Nota Fiscal</span></a></li>
                        @endis

                        <li class="header">Configurações</li>
                        <li><a href="{{ route('profile') }}"><i class="zmdi zmdi-card-travel"></i><span>Perfil</span></a></li>
                        <li><a href="#"><i class="zmdi zmdi-settings"></i><span>Configs Gerais</span></a></li>
                    </ul>
                </div>
                <!-- #Menu -->
            </aside>
            <!-- #END# Left Sidebar --> 
            <!-- Right Sidebar -->
            <aside id="rightsidebar" class="right-sidebar">
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#skins">Skins</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#chat">Chat</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#settings">Setting</a></li>
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane in active in active" id="skins">
                        <ul class="demo-choose-skin">
                            <li data-theme="red">
                                <div class="red"></div>
                                <span>Red</span> </li>
                            <li data-theme="purple">
                                <div class="purple"></div>
                                <span>Purple</span> </li>
                            <li data-theme="blue">
                                <div class="blue"></div>
                                <span>Blue</span> </li>
                            <li data-theme="cyan" class="active">
                                <div class="cyan"></div>
                                <span>Cyan</span> </li>
                            <li data-theme="green">
                                <div class="green"></div>
                                <span>Green</span> </li>
                            <li data-theme="deep-orange">
                                <div class="deep-orange"></div>
                                <span>Deep Orange</span> </li>
                            <li data-theme="blue-grey">
                                <div class="blue-grey"></div>
                                <span>Blue Grey</span> </li>
                            <li data-theme="black">
                                <div class="black"></div>
                                <span>Black</span> </li>
                            <li data-theme="blush">
                                <div class="blush"></div>
                                <span>Blush</span> </li>
                        </ul>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="chat">
                        <div class="demo-settings">
                            <div class="search">
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="text" class="form-control" placeholder="Search..." required autofocus>
                                    </div>
                                </div>
                            </div>
                            <h6>Recent</h6>
                            <ul>
                                <li class="online">
                                    <div class="media">
                                        <a  role="button" tabindex="0"> <img class="media-object " src="/vendor/swift/assets/images/xs/avatar1.jpg" alt=""> </a>
                                        <div class="media-body">
                                            <span class="name">Claire Sassu</span> <span class="message">Can you share the...</span> <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="online">
                                    <div class="media"> <a  role="button" tabindex="0"> <img class="media-object " src="/vendor/swift/assets/images/xs/avatar2.jpg" alt=""> </a>
                                        <div class="media-body">
                                            <span class="name">Maggie jackson</span> <span class="message">Can you share the...</span> <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="online">
                                    <div class="media"> <a  role="button" tabindex="0"> <img class="media-object " src="/vendor/swift/assets/images/xs/avatar3.jpg" alt=""> </a>
                                        <div class="media-body">
                                            <span class="name">Joel King</span> <span class="message">Ready for the meeti...</span> <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <h6>Contacts</h6>
                            <ul>
                                <li class="offline">
                                    <div class="media"> <a  role="button" tabindex="0"> <img class="media-object " src="/vendor/swift/assets/images/xs/avatar4.jpg" alt=""> </a>
                                        <div class="media-body">
                                            <span class="name">Joel King</span> <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="online">
                                    <div class="media"> <a  role="button" tabindex="0"> <img class="media-object " src="/vendor/swift/assets/images/xs/avatar1.jpg" alt=""> </a>
                                        <div class="media-body">
                                            <span class="name">Joel King</span> <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="offline">
                                    <div class="media"> <a class="pull-left " role="button" tabindex="0"> <img class="media-object " src="/vendor/swift/assets/images/xs/avatar2.jpg" alt=""> </a>
                                        <div class="media-body">
                                            <span class="name">Joel King</span> <span class="badge badge-outline status"></span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="settings">
                        <div class="demo-settings">
                            <p>GENERAL SETTINGS</p>
                            <ul class="setting-list">
                                <li> <span>Report Panel Usage</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox" checked>
                                            <span class="lever"></span></label>
                                    </div>
                                </li>
                                <li> <span>Email Redirect</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox">
                                            <span class="lever"></span></label>
                                    </div>
                                </li>
                            </ul>
                            <p>SYSTEM SETTINGS</p>
                            <ul class="setting-list">
                                <li> <span>Notifications</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox" checked>
                                            <span class="lever"></span></label>
                                    </div>
                                </li>
                                <li> <span>Auto Updates</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox" checked>
                                            <span class="lever"></span></label>
                                    </div>
                                </li>
                            </ul>
                            <p>ACCOUNT SETTINGS</p>
                            <ul class="setting-list">
                                <li> <span>Offline</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox">
                                            <span class="lever"></span></label>
                                    </div>
                                </li>
                                <li> <span>Location Permission</span>
                                    <div class="switch">
                                        <label>
                                            <input type="checkbox" checked>
                                            <span class="lever"></span></label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </aside>
            <!-- #END# Right Sidebar --> 
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="block-header">
                    @section('block-header')
                    @show

                    @if ($errors->any())
                        <br />
                        <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(Session::has('message') && Session::has('status') )
                        <br />
                        <div class="alert alert-{{ Session::get('status') }}" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <strong>@if(Session::get('status') == 'danger') Aviso! @else Sucesso! @endif</strong> {{ Session::get('message')}}.
                        </div>
                    @endif
                </div>

                @yield('content')
            </div>
        </section>

        <div class="color-bg"></div>
        <!-- Jquery Core Js --> 
        <script src="/vendor/swift/assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
        <script src="/vendor/swift/assets/bundles/morphingsearchscripts.bundle.js"></script> <!-- morphing search Js --> 
        <script src="/vendor/swift/assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

        <script src="/vendor/swift/assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
        <script src="/vendor/swift/assets/bundles/morphingscripts.bundle.js"></script><!-- morphing search page js -->

        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

        <script>
            $.get('/helper-datas')
             .done(function(response){
                 $('#total').html(response.total_documentos + '<i>Total</i>');
                 $('#nfe').html(response.total_nfe + '<i>NFE</i>');
                 $('#demais').html(response.total_demais + '<i>Demais</i>');

                 $('#hospitals').html(response.total_hospitals + '<i>Hospitais</i>');
                 $('#doctors').html(response.total_doctors + '<i>Médicos</i>');
                 $('#documents').html(response.total_documents + '<i>Arquivos</i>');
             });
        </script>
        @section('js')
        @show
    </body>
</html>
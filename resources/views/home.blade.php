@extends('layouts.base')

@section('content')
    <div class="block-header">
        <h2>Dashboard</h2>
        <small class="text-muted">Bem vindo ao Sistema Martins&Gagliotti</small>
    </div>

    @is('doctor')
        <div class="card card-default">
            <div class="header">
                <h2>
                    Indicadores 2019
                </h2>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped">
                        <thead class="bg-cyan">
                            <th>Competência</th>
                            <th>Qtd. Turnos</th>
                            <th>Valor/Hora</th>
                            <th>Total</th>
                        </thead>
                        <tbody>
                            @foreach($indicator as $item)
                                <tr>
                                    <td>{{ str_pad($item->month, 2, '0', STR_PAD_LEFT) }}</td>
                                    <td>{{ $item->work_shift }}</td>
                                    <td>{{ $item->value_per_shift }}</td>
                                    <td>{{ number_format($item->work_shift * $item->moeda($item->value_per_shift), 2, ',', '.') }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
        <div class="card card-default">
            <div class="header">
                <h2>
                    @if ( Request::input('type') == 'nfe' ) 
                        Notas Fiscais
                    @else 
                        Resultados Gerais
                    @endif
                </h2>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-striped">
                        <thead>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Data</th>
                            <th></th>
                        </thead>
                        <tbody>
                            @foreach($files as $file)
                                <tr>
                                    <td>{{ $file->id }}</td>
                                    <td>{{ $file->name }}</td>
                                    <td>{{ (new \DateTime($file->create_at))->format('d-m-Y H:i') }}</td>
                                    <td class="text-right">
                                        <a role="button" target="_blank" class="btn btn-primary btn-xs btn-raised" href="/doctor/files/{{$file->id}}/download" title="download">download</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="float-right">
                    {!! $files->links('pagination::bootstrap-4') !!}
                </div>
            </div>
        </div>
    @endis

    @is('admin')
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="info-box-4 hover-zoom-effect">
                    <div class="icon"> <i class="zmdi zmdi-account col-blue"></i> </div>
                    <div class="content">
                        <div class="text">Médicos</div>
                        <div class="number">{{ $total_doctors }}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="info-box-4 hover-zoom-effect">
                    <div class="icon"> <i class="zmdi zmdi-file-text col-green"></i> </div>
                    <div class="content">
                        <div class="text">Documentos Enviados</div>
                        <div class="number">{{ $total_documents }}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="info-box-4 hover-zoom-effect">
                    <div class="icon"> <i class="zmdi zmdi-brightness-medium col-blush"></i> </div>
                    <div class="content">
                        <div class="text">Documentos Recebidos</div>
                        <div class="number">{{ $total_productivities }}</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="info-box-4 hover-zoom-effect">
                    <div class="icon"> <i class="zmdi zmdi-developer-board col-cyan"></i> </div>
                    <div class="content">
                        <div class="text">Hospitais</div>
                        <div class="number">{{ $total_hospitals }}</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2> Documentos <small>Ultimos documentos Recebidos</small> </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nome</th>
                                        <th>Médico</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($files_doctor as $file)
                                        <tr>
                                            <td>{{ $file->id }}</td>
                                            <td>{{ $file->name }}</td>
                                            <td>
                                                <a @if($file->owner) href="{{ route('doctors.edit', ['id' => $file->owner->id]) }}" @endif>
                                                    {{  $file->owner ? $file->owner->name : 'não informado' }}
                                                </a>
                                            </td>
                                            <td class="text-right">
                                                <a role="button" target="_blank" class="btn btn-primary btn-xs btn-raised" href="/admin/files/{{$file->id}}/download" title="download">download</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2> Documentos de Resultados <small>Ultimos documentos Recebidos</small> </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>Tipo</th>
                                    <th>Médico</th>
                                    <th>Data</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($files as $file)
                                    <tr>
                                        <td>{{ $file->id }}</td>
                                        <td>{{ $file->name }}</td>
                                        <td>{{ $file->category }}</td>
                                        <td>{{  $file->owner ? $file->owner->name : 'não informado' }}</td>
                                        <td class="text-right">
                                            <a role="button" target="_blank" class="btn btn-primary btn-xs btn-raised" href="/admin/files/{{$file->id}}/download" title="download">download</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endis
@endsection

@extends('layouts.base')

@section('content')
    <div class="card">
        <div class="header">
            <h2>
                <i class="fa fa-fw fa-file-excel-o"></i>
                Formulário de adição de novo arquivo
            </h2>
        </div>
        <div class="body">
            <form action="{{ route('doctor.files.store') }}" enctype="multipart/form-data" method="POST">
                {{ csrf_field() }}
                <select class="form-control" id="owner" name="owner" style="display:none;" readonly required>
                    <option value="{{ Auth::user()->doctor->id }}" selected>{{ Auth::user()->doctor->name }}</option>
                </select>
                <select class="form-control" id="category" name="category" style="display:none;" readonly required>
                    <option value="DOCTOR" selected>DOCUMENTO</option>
                </select>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="name" >Nome do Arquivo</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="arquivo.pdf" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group float-right">
                            <input type="file" class="form-control-file" id="file" name="file" required>
                        </div>
                    </div>
                </div>

                <div class="float-right">
                    <button type="submit" class="btn btn-primary btn-raised">
                        Upload
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script 
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js" 
        integrity="sha256-PtzTX1ftmEmj8YUiAX0wTIQ+ddTAGVt2MiLMsGsAMxM=" 
        crossorigin="anonymous"></script>
    <script src="/js/bootstrap-fileselect.js"></script>
    <script>
        $('#file').fileselect({
            language: "en",
            browseBtnClass: 'btn btn-danger btn-raised'
        });
    </script>
@stop
@extends('layouts.base')


@section('content')
    <div class="card card-default">
        <div class="header">
            <h2>
                Documentos Adicionados
                <a role="button" class="btn btn-sm btn-raised btn-primary float-right" href="{{ route('doctor.files.create') }}">
                    Adicionar Documento
                </a>
            </h2>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-sm table-bordered table-striped">
                    <thead>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Data</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach($files as $file)
                            <tr>
                                <td>{{ $file->id }}</td>
                                <td>{{ $file->name }}</td>
                                <td>{{ (new \DateTime($file->create_at))->format('d-m-Y H:i') }}</td>
                                <td class="text-right">
                                    <a role="button" target="_blank" class="btn btn-primary btn-xs btn-raised" href="/doctor/files/{{$file->id}}/download" title="download">download</a>
                                    <button type="button" class="btn btn-danger btn-xs btn-raised" onclick="deleteFile({{$file->id}})">apagar</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            {!! $files->links() !!}
        </div>
    </div>

    <form id="delete_form" method="post">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
    </form>
@stop

@section('js')
    <script src="{{ asset('js/doctor.files.js') }}"></script>
@stop
<?php 

namespace Healtho\Repositories;

use Healtho\Repositories\Interfaces\BaseRepositoryInterface;

class BaseRepository implements BaseRepositoryInterface {
    protected $model;

    public function find($id) {
        return $this->model->findOrFail( $id );
    }

    public function findAll() {
        return $this->model->all();
    }

    public function findAllPaginate($num=10)
    {
        return $this->model->paginate($num);
    }

    /**
     * TODO: Adicionar Validações
     */
    public function update($id, $data) {
        $entity = $this->model->findOrFail( $id );
        $entity->fill( $data );
        $entity->save();

        return $entity;
    }

    /**
     * TODO: Adicionar Validações
     */
    public function create($data) {
        $entity = $this->model->create( $data );
        $entity->save();

        return $entity;
    }

    public function delete($id) {
        $entity = $this->model->findOrFail( $id );
        $entity->delete();
    }
}
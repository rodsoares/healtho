<?php

namespace Healtho\Repositories;

use Healtho\Models\Hospital;
use Healtho\Repositories\Interfaces\HospitalRepositoryInterface;

class HospitalRepository extends BaseRepository implements HospitalRepositoryInterface {
    protected $model;

    public function __construct(Hospital $hospitalModel) {
        $this->model = $hospitalModel;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: rodpluto
 * Date: 13/02/19
 * Time: 20:11
 */

namespace Healtho\Repositories;

use Illuminate\Support\Facades\Storage;
use Healtho\Models\Doctor;
use Healtho\Models\File;
use Healtho\Models\User;
use Healtho\Repositories\Interfaces\UserRepositoryInterface;
use Artesaos\Defender\Facades\Defender;


class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    protected $model;

    public function __construct(User $model) {
        $this->model = $model;
    }

    public function create($data) {
        $entity = $this->model->create( $data );
        $entity->save();

        $roleDoctor = Defender::findRole('admin');

        $entity->attachRole($roleDoctor);

        $entity->save();

        return $entity;
    }

    public function update($id, $data) {
        $entity = $this->model->findOrFail($id);

        if ( isset( $data['password'] ) ) {
            $entity->password = bcrypt( $data['password']);
            unset($data['password']);
        } else {
            unset($data['password']);
            unset($data['password_confirmation']);
        }

        $entity->fill( $data );
        $entity->syncRoles([ $data['role_id'] ]);

        $entity->save();
    }

    public function delete($id) {
        $entity = $this->model->findOrFail($id);

        if( $entity->roles[0]->name == 'doctor' ) {
            $doctor = Doctor::findOrFail( $entity->doctor->id );
            foreach( $doctor->files as $file ) {
                $aux = File::findOrFail( $file->id );
                Storage::delete($file->path);
                $aux->delete();
            }
            $doctor->delete();
        }
        $entity->delete();
    }
}
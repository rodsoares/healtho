<?php

namespace Healtho\Repositories\Interfaces;

interface BaseRepositoryInterface {
    public function find($id);
    public function findAll();
    public function findAllPaginate($num);
    public function update($id, $data);
    public function create($data);
    public function delete($id);
}
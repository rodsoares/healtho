<?php

namespace Healtho\Repositories;

use Healtho\Models\Doctor;
use Healtho\Models\User;
use Healtho\Repositories\Interfaces\DoctorRepositoryInterface;
use Artesaos\Defender\Facades\Defender;


class DoctorRepository extends BaseRepository implements DoctorRepositoryInterface {
    protected $model;

    public function __construct(Doctor $model) {
        $this->model = $model;
    }

    public function create($data) {
        $entity = $this->model->create( $data );
        $entity->save();

        $roleDoctor = Defender::findRole('doctor');
        
        // criando usuário
        if ( $entity ) {
            $user = User::create([
                'name' => $entity->name,
                'email' => $entity->email,
                'password' => bcrypt( $data['password'] )
            ]);
            $user->attachRole($roleDoctor);
            $user->save();

            $entity->user_id = $user->id;
            $entity->save();
        } else {
            throw new \Exception();
        }

        return $entity;
    }

    public function update($id, $data) {
        $entity = $this->model->findOrFail($id);
        $entity->fill( $data );

        if ( isset( $data['photo'] ) ) {
           $entity->user->photo = $data['photo'];
           $entity->user->save();
        }

        if ( $entity->save() ) {
            $user = User::findOrFail( $entity->user->id );
            $user->name = $entity->name;
            $user->email = $entity->email;
            if ( isset( $data['password'] ) ) {
                $user->password = bcrypt( $data['password']);
                $user->save();
                
            }
            $user->save();
            return true;
        } else {
            return false;
        }
    }
}
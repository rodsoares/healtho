<?php

namespace Healtho\Http\Controllers;

use Illuminate\Http\Request;
use Healtho\Models\User;
use Healtho\Models\File;
use Healtho\Models\Doctor;
use Healtho\Models\Hospital;
use Healtho\Models\PerformanceIndicator as Indicator;

use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ( Auth::user()->doctor ){
            
            $files = File::where([
                ['doctor_id', Auth::user()->doctor->id],
                ['category','!=', 'DOCTOR']
            ])->orderBy('id')->paginate(15);
            $indicator = Indicator::where([
                ['doctor_id', '=', Auth::user()->doctor->id],
                ['year', '=', date('Y')]
            ])->get(); 

            //return ( $indicator );
            if ( $request->has('type') ) {
                switch( $request->input('type') ){
                    case 'all': 
                        $files = File::where([
                            ['doctor_id', Auth::user()->doctor->id],
                            ['category','!=', 'DOCTOR'],
                            ['category','!=', 'NFE']
                        ])->orderBy('id')->paginate(15);
                        break;
                    case 'nfe':
                        $files = File::where([
                            ['doctor_id', '=' , Auth::user()->doctor->id],
                            ['category', '=', 'NFE']
                            ])->orderBy('id')->paginate(15);
                        break;  
                }
            }
        
            return view('home', compact('files', 'indicator'));
        } else {
            return view('home', [
                'files_doctor' => File::where('category', 'DOCTOR')->limit(5)->orderBy('created_at', 'DESC')->get(),
                'files' => File::where('category','!=', 'DOCTOR')->limit(5)->orderBy('created_at', 'DESC')->get(),
                'total_hospitals' => count( Hospital::all() ),
                'total_documents' => count( File::where('category','!=', 'DOCTOR')->get() ),
                'total_doctors' => count( Doctor::all() ),
                'total_productivities' => count( File::where('category','=', 'DOCTOR')->get() )
            ]);
        }
    }

    public function helper_data() {
        if( Auth::user()->doctor ) {
            $total = File::where([
                ['doctor_id', '=', Auth::user()->doctor->id]
            ])->get();
            $total_documentos = count( $total );

            $total = File::where([
                ['doctor_id', '=', Auth::user()->doctor->id],
                ['category', '=', 'NFE']
            ])->get();
            $total_nfe = count($total);

            $total = File::where([
                ['doctor_id', '=', Auth::user()->doctor->id],
                ['category', '!=', 'NFE']
            ])->get();
            $total_demais = count($total);   

            $total_hospitals = null;
            $total_doctors = null;
            $total_documents = null;
        } else {
            $total_documentos = null;
            $total_nfe = null;
            $total_demais = null; 

            $total_hospitals = count(Hospital::all());
            $total_doctors = count(Doctor::all());
            $total_documents = count(File::all());
        }

        return response()->json([
           'total_documentos' => $total_documentos,
           'total_nfe' => $total_nfe,
           'total_demais' => $total_demais,
           'total_hospitals' => $total_hospitals,
           'total_doctors' => $total_doctors,
           'total_documents' => $total_documents,
        ]);
    }
}

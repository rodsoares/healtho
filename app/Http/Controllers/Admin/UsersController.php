<?php

namespace Healtho\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Artesaos\Defender\Facades\Defender;
use Artesaos\Defender\Role;
use Healtho\Models\User;
use Healtho\Repositories\Interfaces\UserRepositoryInterface;
use Healtho\Http\Controllers\Controller;

class UsersController extends Controller
{

    private $userRepo;

    public function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepo = $userRepo;

        $this->middleware('needsRole:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $query = [];
        if ( $request->input('email') ) {
            $query[] = ['email', '=', $request->input('email')]; 
        }
        if ( $request->input('name') ) {
            $query[] = ['name', 'like', '%'.$request->input('name').'%'];
        }

        if ( $request->input('role') ) {
            $users = User::whereHas('roles', function($q) use($request) { // Esse whereHas é uma mão da roda quadrada do preguiçoso!
                $q->where('name', $request->input('role')); // Muito massa isso aqui hein! Laravel Mandando ver em tudo!
            })->where( $query )->orderBy('created_at', 'DESC')->paginate(10);
        } else {
            $users = User::where( $query )->orderBy('created_at', 'DESC')->paginate(10);
        }

        $roles = Defender::rolesList();
        return view('admin.users.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: Retirar esse processamento daqui.
        $dataValidated = $request->validate([
            'name' => 'bail|required|max:255',
            'email' => 'required|email|string',
            'role_id' => 'required',
            'password' => 'required|string',
            'password_confirmation' => 'same:password',
        ]);

       try {
            $data = $dataValidated;
            $data['password'] = bcrypt( $data['password']);
            $this->userRepo->create( $data );
            return redirect()->route('users.index')->with([
                'status' => 'success',
                'message' => 'Usuário Administrativo adicionado com sucesso!'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('users.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Role::all();
        $user  = $this->userRepo->find($id);
        return view('admin.users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // TODO: Retirar esse processamento daqui.
        $dataValidated = $request->validate([
            'name' => 'nullable|max:255',
            'role_id' => 'nullable',
            'email' => 'nullable|string',
            'password' => 'nullable|string',
            'password_confirmation' => 'same:password',
        ]);


        try {
            $data = $dataValidated;
            $this->userRepo->update( $id, $data );
            return redirect()->route('users.index')->with([
                'status' => 'success',
                'message' => 'Usuário atualizado com sucesso!'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('users.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->userRepo->delete($id);
            return redirect()->route('users.index')->with([
                'status' => 'success',
                'message' => 'Usuário deletado da base de dados com sucesso!'
            ]);
        } catch (\Exception $e) {
            return redirect()->route('users.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }
}

<?php

namespace Healtho\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Healtho\Http\Controllers\Controller;

use Healtho\Models\Hospital;
use Healtho\Repositories\Interfaces\HospitalRepositoryInterface as HospitalInterface;

class HospitalsController extends Controller
{
    protected $hospitalRepo;

    public function __construct(HospitalInterface $repo)
    {
        $this->middleware('needsRole:admin');
        $this->hospitalRepo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('toDataTables') && $request->isMethod('GET') ) {
            return response()->json( 
                $this->dtFormat(
                    $this->hospitalRepo->findAll()
                    )
                );
        }
        $hospitals = Hospital::paginate(10);
        return view('admin.hospitals.index', compact('hospitals'));
    }

    public function create() {
        return view('admin.hospitals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: Retirar esse processamento daqui.
        $dataValidated = $request->validate([
            'name' => 'bail|required|unique:hospitals|max:255',
            'status' => 'required|boolean',
        ]);

        try {
            $data = $dataValidated;
            $this->hospitalRepo->create( $data );
            return redirect()->route('hospitals.index')->with([
                'status' => 'success',
                'message' => 'Hospital adicionado com sucesso!'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('hospitals.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $hospital = $this->hospitalRepo->find( $id );
            return view('admin.hospitals.edit', compact('hospital'));
        } catch(ModelNotFoundException $e) {
            return redirect()->route('hospitals.index')->with([
                'status' => 'danger',
                'message' => 'Registro inexistente na base de dados.'
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // TODO: Retirar esse processamento daqui.
        $dataValidated = $request->validate([
            'name' => 'bail|required|unique:hospitals|max:255',
            'status' => 'required|boolean',
        ]);

        try {
            $data = $dataValidated;
            $this->hospitalRepo->update($id, $data );
            return redirect()->route('hospitals.index')->with([
                'status' => 'success',
                'message' => 'Hospital editado com sucesso!'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('hospitals.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->hospitalRepo->delete($id);
            return redirect()->route('hospitals.index')->with([
                'status' => 'success',
                'message' => 'Hospital apagado com sucesso!'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('hospitals.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }

    /**
     * Formatador de dados para o data tables
     */
    private function dtFormat( $data ) {
        $response = [];
        $response['data'] = [];

        foreach ($data as $item) {
            $item->action_buttons = '<a role="button" class="btn btn-primary btn-xs" href="/admin/hospitals/'.$item->id.'/edit" style="margin-right: 6px"><i class="fa fa-fw fa-edit"></i> editar</a>';
            $item->action_buttons .= '<button type="button" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash-o"></i> apagar</button>';

            $response['data'][] = [
                $item->id,
                $item->status ? 'ATIVO': 'INATIVO',
                $item->name,
                $item->action_buttons
            ];
        }

        return $response;
    }
}

<?php

namespace Healtho\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
 
use Healtho\Http\Controllers\Controller;

use Healtho\Models\Doctor;
use Healtho\Models\Hospital;

use Healtho\Repositories\Interfaces\DoctorRepositoryInterface as DocRepository;
use Healtho\Repositories\Interfaces\HospitalRepositoryInterface as HospitalRepository;

class DoctorsController extends Controller
{
    private $docRepo;
    private $hospRepo;

    public function __construct(
        DocRepository $doc,
        HospitalRepository $hosp
    )
    {
        $this->docRepo = $doc;    
        $this->hospRepo = $hosp;
        
        $this->middleware('needsRole:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = [];
        if ( $request->input('email') ) {
            $query[] = ['email', '=', $request->input('email')]; 
        }
        if ( $request->input('name') ) {
            $query[] = ['name', 'like', '%'.$request->input('name').'%'];
        }

        if ( $request->input('hospital') ) {    
            $query[] = ['hospital_id', '=', $request->input('hospital')];     
        }

        $doctors   = Doctor::where( $query )->orderBy('created_at', 'DESC')->paginate(12);
        $hospitals = Hospital::all(); 

        return view('admin.doctors.index', compact('doctors', 'hospitals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hospitals = $this->hospRepo->findAll();
        return view('admin.doctors.create', compact('hospitals'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: Retirar esse processamento daqui.
        $dataValidated = $request->validate([
            'name' => 'bail|required|max:255',
            'speciality' => 'required|string|max:255',
            'hospital_id' => 'required',
            'cpf' => 'required|unique:doctors|string',
            'crm' => 'required|unique:doctors|string',
            'started_in' => 'required|string',
            'cellphone_number' => 'required|string',
            'email' => 'required|unique:users,email|string',
            'address' => 'required|string',
            'mother' => 'nullable|string',
            'pis_nit' => 'nullable|string',
            'rg' => 'nullable|string',
            'bank' => 'nullable|string',
            'bank_branch' => 'nullable|string',
            'checking_account' => 'nullable|string',
            'marital_status' => 'nullable|string',
            'nationality' => 'nullable|string',
            'password' => 'required|string',
            'password_confirmation' => 'same:password',
        ]);
 
        try {
            $data = $dataValidated;
            $this->docRepo->create( $data );
            return redirect()->route('doctors.index')->with([
                'status' => 'success',
                'message' => 'Médico adicionado com sucesso!'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('doctors.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $hospitals = $this->hospRepo->findAll();
            $doctor = $this->docRepo->find( $id );
            return view('admin.doctors.edit', compact('hospitals', 'doctor'));
        } catch(ModelNotFoundException $e) {
            return redirect()->route('doctors.index')->with([
                'status' => 'danger',
                'message' => 'Registro inexistente na base de dados.'
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // TODO: Retirar esse processamento daqui.
        $dataValidated = $request->validate([
            'name' => 'bail|required|max:255',
            'speciality' => 'required|string|max:255',
            'hospital_id' => 'required',
            'cpf' => 'required|string',
            'crm' => 'required|string',
            'started_in' => 'required|string',
            'cellphone_number' => 'required|string',
            'email' => 'required|string',
            'address' => 'required|string',
            'mother' => 'nullable|string',
            'pis_nit' => 'nullable|string',
            'rg' => 'nullable|string',
            'bank' => 'nullable|string',
            'bank_branch' => 'nullable|string',
            'checking_account' => 'nullable|string',
            'marital_status' => 'nullable|string',
            'nationality' => 'nullable|string',
            'password' => 'nullable|string',
            'password_confirmation' => 'same:password',
        ]);

        try {
            $data = $dataValidated;
            $this->docRepo->update( $id, $data );
            return redirect()->route('doctors.index')->with([
                'status' => 'success',
                'message' => 'Médico atualizado com sucesso!'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('doctors.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->docRepo->delete($id);
            return redirect()->route('doctors.index')->with([
                'status' => 'success',
                'message' => 'Médico atualizado com sucesso!'
            ]);
        }catch(\Exception $e) {
            return redirect()->route('doctors.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }

    /**
     * Formatador de dados para o data tables
     */
    private function dtFormat( $data ) {
        $response = [];
        $response['data'] = [];

        foreach ($data as $item) {
            $item->action_buttons = '<a role="button" class="btn btn-primary btn-xs" href="/admin/files" title="arquivos" style="margin-right: 3px"><i class="fa fa-fw fa-download"></i></a>';
            $item->action_buttons .= '<a role="button" class="btn btn-primary btn-xs" href="/admin/doctors/'.$item->id.'/edit" title="editar" style="margin-right: 3px"><i class="fa fa-fw fa-edit"></i></a>';
            $item->action_buttons .= '<button role="button" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash" title="apagar"></i> apagar</button>';

            $response['data'][] = [
                $item->id,
                $item->name,
                $item->crm,
                $item->hospital ? $item->hospital->name : 'Não Informado',
                $item->action_buttons
            ];
        }

        return $response;
    }
}

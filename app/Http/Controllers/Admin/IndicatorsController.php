<?php

namespace Healtho\Http\Controllers\Admin;

use Healtho\Models\Doctor;
use Healtho\Models\PerformanceIndicator as Indicator;
use Illuminate\Http\Request;
use Healtho\Http\Controllers\Controller;

use Auth;

class IndicatorsController extends Controller
{
    public function __construct() {
        $this->middleware('needsRole:admin');
    }

    public function index() {
        $indicators = Indicator::all();
        return view('admin.indicators.index', compact('indicators'));
    }

    public  function create() {
        $doctors = Doctor::all();
        return view('admin.indicators.create', compact('doctors'));
    }

    public function store(Request $request) {
        try {
            Indicator::create( $request->all() );
            return redirect()->route('indicators.index')->with([
                'status' => 'success',
                'message' => 'Indicador adicionado com sucesso!'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('indicators.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }

    public function destroy($id) {
        $indicator = Indicator::findOrFail( $id );
        $indicator->delete();
        return redirect()->route('indicators.index')->with([
            'status' => 'success',
            'message' => 'Indicador apagado da base de dados'
        ]);
    }
}

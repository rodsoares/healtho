<?php

namespace Healtho\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Healtho\Http\Controllers\Controller;

use Healtho\Models\Doctor;
use Healtho\Models\File;

use Auth;

class FilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('needsRole:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = [];
        $query[] = ['category','!=', 'DOCTOR'];

        /**
         * Filtrando valores baseados nos filtros fornecidos
         */
        if ( $request->input('document_category') ) {
            $query[] = ['category', '=', $request->input('document_category')]; 
        }
        if ( $request->input('doctor') ) {
            $query[] = ['doctor_id', '=', $request->input('doctor')];
        }
        if ( $request->input('document_name') ) {
            $query[] = ['name', 'like', '%'.$request->input('document_name').'%'];
        }

        $doctors = Doctor::all();// usar repositorio!
        $files = File::where($query)->paginate(10);

        return view('admin.files.index', compact('files', 'doctors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $doctors = Doctor::all();// usar repositorio!
        return view('admin.files.create', compact('doctors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $file = $request->file;
            $pfile = new File();
            $pfile->doctor_id = $request->input('owner');
            $pfile->category = $request->input('category');
            $pfile->name = $request->input('name');
            $pname = 'documento-'. date('dmYHis') .'-'. Auth::user()->id .'-'. $pfile->doctor_id .'.'. $file->extension();
            $pfile->path = $file->storeAs('documentos/'.$request->input('category'), $pname);
            $pfile->save();

            return redirect()->route('files.index')->with([
                'status' => 'success',
                'message' => 'Documento adicionado com sucesso!'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('files.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $pfile = File::findOrFail( $id );

            /** deletando arquivos associados do servidor */
            Storage::delete($pfile->path);

            $pfile->delete();
            return redirect()->route('files.index')->with([
                'status' => 'success',
                'message' => 'Documento deletado com sucesso !'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('files.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }

    public function download($id)
    {
        $pfile = File::findOrFail( $id );
        return Storage::download($pfile->path);
    }

    /**
     * Formatador de dados para o data tables
     */
    private function dtFormat( $data ) {
        $response = [];
        $response['data'] = [];

        foreach ($data as $item) {
            $item->action_buttons = '<a role="button" target="_blank" class="btn btn-primary btn-xs" href="/admin/files/'.$item->id.'/download" title="download" style="margin-right: 3px"><i class="fa fa-fw fa-download"></i> download</a>';
            $item->action_buttons .= '<button type="button" class="btn btn-danger btn-xs" onclick="deleteFile('.$item->id.')"><i class="fa fa-fw fa-trash" title="apagar"></i> apagar</button>';

            $response['data'][] = [
                $item->id,
                $item->name,
                $item->category,
                $item->owner->name,
                $item->action_buttons
            ];
        }

        return $response;
    }
}

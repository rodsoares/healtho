<?php

namespace Healtho\Http\Controllers;

use Artesaos\Defender\Role;
use Healtho\Models\Doctor;
use Healtho\Models\Hospital;
use Healtho\Models\User;
use Illuminate\Http\Request;

use Healtho\Repositories\Interfaces\DoctorRepositoryInterface as DocRepository;
use Healtho\Repositories\Interfaces\UserRepositoryInterface as UserRepository;

use Auth;

class ProfileController extends Controller
{
    private $userRepo;
    private $docRepo;

    public function __construct(
        DocRepository $doc,
        UserRepository $user
    )
    {
        $this->docRepo = $doc;
        $this->userRepo = $user;
    }

    public function profile() {
        $user = User::findOrFail( Auth::user()->id );
        $hospitals = Hospital::all();
        $roles = Role::all();
        $doctor = $user;

        if ( Auth::user()->doctor ) {
            $doctor = Doctor::findOrFail( $user->doctor->id );
        }
        return view('profile', compact('user', 'doctor', 'hospitals', 'roles'));
    }

    public function update(Request $request)
    {
        if (Auth::user()->doctor) {
            // TODO: Retirar esse processamento daqui.
            $dataValidated = $request->validate([
                'name' => 'bail|required|max:255',
                'speciality' => 'required|string|max:255',
                'hospital_id' => 'required',
                'cpf' => 'required|string',
                'crm' => 'required|string',
                'phone_number' => 'required|string',
                'cellphone_number' => 'required|string',
                'email' => 'required|string',
                'address' => 'required|string',
                'mother' => 'nullable|string',
                'pis_nit' => 'nullable|string',
                'rg' => 'nullable|string',
                'bank' => 'nullable|string',
                'bank_branch' => 'nullable|string',
                'checking_account' => 'nullable|string',
                'marital_status' => 'nullable|string',
                'nationality' => 'nullable|string',
                'password' => 'nullable|string',
                'password_confirmation' => 'same:password',
            ]);

            try {
                $data = $dataValidated;
                if ( $request->hasFile('photo') ) {
                    $data['photo'] = $this->updatePhoto( $request->file('photo') );
                }
                $this->docRepo->update(Auth::user()->doctor->id, $data);
                return redirect()->route('profile')->with([
                    'status' => 'success',
                    'message' => 'Perfil atualizado com sucesso!'
                ]);
            } catch (\Exception $e) {
                return redirect()->route('profile')->with([
                    'status' => 'danger',
                    'message' => 'Ocorreu algum erro. Tente novamente.'
                ]);
            }
        } else {
            // TODO: Retirar esse processamento daqui.
            $dataValidated = $request->validate([
                'name' => 'nullable|max:255',
                'role_id' => 'nullable',
                'email' => 'nullable|string',
                'password' => 'nullable|string',
                'password_confirmation' => 'same:password',
            ]);


            try {
                $data = $dataValidated;
                if ( $request->hasFile('photo')) {
                    $data['photo'] = $this->updatePhoto( $request->file('photo') );
                }
                $this->userRepo->update( Auth::user()->id, $data );
                return redirect()->route('profile')->with([
                    'status' => 'success',
                    'message' => 'Usuário atualizado com sucesso!'
                ]);
            } catch(\Exception $e) {
                return redirect()->route('profile')->with([
                    'status' => 'danger',
                    'message' => 'Ocorreu algum erro. Tente novamente.'
                ]);
            }
        }
    }

    private function updatePhoto( $photo ) {
        $pname = 'doctor-'. Auth::user()->id .'.'. $photo->extension();
        return $photo->storeAs('doctors',$pname, 'profile_photo');
    }
}

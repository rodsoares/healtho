<?php

namespace Healtho\Http\Controllers\Doctor;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Healtho\Http\Controllers\Controller;

use Healtho\Models\File;

use Auth;

class FilesController extends Controller
{
    public function __construct() {
        $this->middleware('needsRole:doctor');
    }

    public function index() {
        $files = File::where([
            ['category', 'DOCTOR'],
            ['doctor_id', Auth::user()->doctor->id]
        ])->paginate(10);
        return view('doctor.files.index', compact('files'));
    }

    public function create(Request $request) {
        return view('doctor.files.create');
    }

    public function store(Request $request) {
        try {
            $file = $request->file;
            $pfile = new File();
            $pfile->doctor_id = $request->input('owner');
            $pfile->category = $request->input('category');
            $pfile->name = $request->input('name');
            $pname = 'documento-'. date('dmYHis') .'-'. Auth::user()->id .'-'. $pfile->doctor_id .'.'. $file->extension();
            $pfile->path = $file->storeAs('documentos/'.$request->input('category'), $pname);
            $pfile->save();

            return redirect()->route('doctor.files.index')->with([
                'status' => 'success',
                'message' => 'Documento adicionado com sucesso!'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('doctor.files.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }

    public function destroy($id) {
        try {
            $pfile = File::findOrFail( $id );

            /** deletando arquivos associados do servidor */
            Storage::delete($pfile->path);

            $pfile->delete();
            return redirect()->route('doctor.files.index')->with([
                'status' => 'success',
                'message' => 'Documento deletado com sucesso !'
            ]);
        } catch(\Exception $e) {
            return redirect()->route('doctor.files.index')->with([
                'status' => 'danger',
                'message' => 'Ocorreu algum erro. Tente novamente.'
            ]);
        }
    }

    public function download($id)
    {
        $pfile = File::findOrFail( $id );
        return Storage::download($pfile->path);
    }
}

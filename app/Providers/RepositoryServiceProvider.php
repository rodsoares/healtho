<?php

namespace Healtho\Providers;

use Illuminate\Support\ServiceProvider;

use Healtho\Repositories\Interfaces\HospitalRepositoryInterface;
use Healtho\Repositories\HospitalRepository;

use Healtho\Repositories\Interfaces\DoctorRepositoryInterface;
use Healtho\Repositories\DoctorRepository;

use Healtho\Repositories\Interfaces\UserRepositoryInterface;
use Healtho\Repositories\UserRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        // Registrando repositórios

        $this->app->bind(
            HospitalRepositoryInterface::class,
            HospitalRepository::class
        );

        $this->app->bind(
            DoctorRepositoryInterface::class,
            DoctorRepository::class
        );

        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
    }
}

<?php

namespace Healtho\Models;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable = [
        'hospital_id',
        'user_id',
        'name',
        'speciality',
        'cpf',
        'crm',
        'mother',
        'pis_nit',
        'rg',
        'bank',
        'bank_branch',
        'checking_account',
        'marital_status',
        'nationality',
        'started_in',
        'cellphone_number',
        'email',
        'address'
    ];

    public function hospital() {
        return $this->belongsTo(Hospital::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function files() {
        return $this->hasMany(File::class);
    }

    public function indicators() {
        return $this->hasMany(PerformanceIndicator::class, 'doctor_id');
    }
}

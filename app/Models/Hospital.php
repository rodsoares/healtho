<?php

namespace Healtho\Models;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    protected $fillable = [ 'name', 'status' ];
}

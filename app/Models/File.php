<?php

namespace Healtho\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public function owner() {
        return $this->belongsTo(Doctor::class, 'doctor_id');
    }
}

<?php

namespace Healtho\Models;

use Illuminate\Database\Eloquent\Model;

class PerformanceIndicator extends Model
{
    protected $fillable = [
        'doctor_id',
        'month',
        'year',
        'work_shift',
        'value_per_shift'
    ];

    public function doctor() {
        return $this->belongsTo(Doctor::class, 'doctor_id');
    }
    
    public function moeda($get_valor) {
        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
        return $valor; //retorna o valor formatado para gravar no banco
    }
}

$(function() {

});

function deleteUser( id ) {
    swal({
        title: "Você tem certeza?",
        text: "Após essa ação o registro será deletado permanentemente?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then(willDelete => {
          if ( willDelete ) {
              $('#user-'+id).submit();
          }
      })
}
$(function() {
    // Setup - add a text input to each footer cell
    $('table[name="tbl-doctors"] thead th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );

    var table = $('table[name="tbl-doctors"]').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: "/admin/doctors/?toDataTables",
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        },
        "columnDefs": [
            { "width": "5%", "targets": 0 },
            { "width": "15%", "targets": 4 }
          ]
    });  
    
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
});

function deleteDoctor( id ) {
    swal({
        title: "Você tem certeza?",
        text: "Após essa ação o registro será deletado permanentemente?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then(willDelete => {
          if ( willDelete ) {
              $('#doctor-'+id).submit();
          }
      })
}
function deleteFile(id){
    if ( confirm('Você tem certeza dessa ação ?')) {
        $('#delete_form').attr('action', '/doctor/files/'+id);
        $('#delete_form').submit();
    }
}


$(function() {
    // Setup - add a text input to each footer cell
    $('table[name="tbl-files"] thead th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );

    var table = $('table[name="tbl-files"]').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: "/admin/files/?toDataTables",
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        }
    });  
    
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
});
$(function() {
    // Setup - add a text input to each footer cell
    $('table[name="tbl-hospitals"] thead th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );

    var table = $('table[name="tbl-hospitals"]').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        ajax: "/admin/hospitals/?toDataTables",
        language: {
            url: "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        },
        "columnDefs": [
            { "width": "5%", "targets": 0 },
            { "width": "10%", "targets": 1 },
            { "width": "20%", "targets": 3 }
          ]
    });  
    
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
});

function deleteHospital( id ) {
    swal({
        title: "Você tem certeza?",
        text: "Após essa ação o registro será deletado permanentemente?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then(willDelete => {
          if ( willDelete ) {
              $('#hospital-'+id).submit();
          }
      })
}

function openAddModal() {
    $('#add_form_modal').modal('show');
}

function openEditModal() {
    $('#edit_form_modal').modal('show');
}
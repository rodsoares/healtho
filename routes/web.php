<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/helper-datas', 'HomeController@helper_data');

Route::get('/profile', 'ProfileController@profile')->name('profile');
Route::put('/profile', 'ProfileController@update')->name('profile.update');

Route::prefix('admin')->namespace('Admin')->middleware(['auth'])->group(function() {
    Route::get('files/{file}/download', 'FilesController@download');

    Route::resource('doctors', 'DoctorsController');
    Route::resource('files', 'FilesController');
    Route::resource('hospitals', 'HospitalsController');
    Route::resource('users', 'UsersController');
    Route::resource('indicators', 'IndicatorsController');
});

Route::prefix('doctor')->namespace('Doctor')->middleware(['auth'])->group(function() {
    Route::get('files/{file}/download', 'FilesController@download')->name('doctor.file-download');
    Route::resource('files', 'FilesController', ['as' => 'doctor']);
});
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('hospital_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('speciality');
            $table->string('cpf');
            $table->string('crm');
            $table->string('started_in')->nullable();
            $table->string('cellphone_number')->nullable();
            $table->string('email');
            $table->string('zipcode')->nullable();
            $table->string('state',2)->nullable();
            $table->string('city')->nullable();
            $table->string('address')->nullable();
            $table->string('pis_nit')->nullable();
            $table->string('rg')->nullable();
            $table->string('mother')->nullable();
            $table->string('bank')->nullable();
            $table->string('bank_branch')->nullable();
            $table->string('checking_account')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('nationality')->nullable();
            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')
                  ->on('users');

            $table->foreign('hospital_id')
                  ->references('id')
                  ->on('hospitals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}

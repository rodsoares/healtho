<?php

use Illuminate\Database\Seeder;
use Healtho\Models\User;
use Artesaos\Defender\Facades\Defender;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $radmin = Defender::createRole('admin');
        $rdoctor = Defender::createRole('doctor');

        $user = User::create([
            'name' => 'Usuário Administrativo',
            'email' => 'admin@admin.com',
            'password' => bcrypt(123456789)
        ]);

        $user->attachRole( $radmin );
    }
}

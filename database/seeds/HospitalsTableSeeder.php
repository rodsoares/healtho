<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Healtho\Models\Hospital;

class HospitalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($cont=1; $cont <= 50; $cont++) {
            DB::table('hospitals')->insert([
                'name'   => "Hospital Teste #$cont",
                'status' => true
            ]);
        }
    }
}
